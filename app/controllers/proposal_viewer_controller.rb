class ProposalViewerController < ApplicationController
  def show
    proposal = Proposal.find(params[:id]).decorate
    service = ProposalViewerService.new(proposal)

    render text: service.render
  end


end
