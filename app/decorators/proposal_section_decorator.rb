class ProposalSectionDecorator < ApplicationDecorator
  decorates_association :proposal
end
