class ProposalDecorator < ApplicationDecorator
  decorates_association :client
  decorates_association :proposal_sections

  def send_date
    localize object.send_date, format: :long
  end
end
