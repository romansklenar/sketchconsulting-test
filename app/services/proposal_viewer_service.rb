require 'nokogiri'

class ProposalViewerService < ApplicationService
  attr_accessor :proposal, :document

  def initialize(proposal)
    @proposal = proposal
  end

  def document
    @document ||= Nokogiri::HTML File.open("#{Rails.root}/public/proposal-template/index.html")
  end

  def render
    assign_base
    assign_client_variables
    assign_proposal_variables
    assign_proposal_sections_variables
    document.to_html
  end


  private

    def assign_base
      base = Nokogiri::XML::Node.new("base", document)
      base['href'] = '/proposal-template/'
      document.search('head').children.before(base)
    end

    def assign_client_variables
      assign_variables_to_node({
        'client_company' => proposal.client.company,
        'client_name'    => proposal.client.name,
        'client_website' => proposal.client.website
      }, document)
    end

    def assign_proposal_variables
      assign_variables_to_node({
        'proposal_name'      => proposal.name,
        'proposal_send_date' => proposal.send_date,
        'proposal_user_name' => proposal.user_name
      }, document)
    end

    def assign_proposal_sections_variables
      section_node = document.search('#proposal-content/div:first()').first.clone
      proposal.proposal_sections.each do |section|
        dolly = section_node.clone
        assign_variables_to_node({
          'section_header'  => section.name,
          'section_content' => section.description
        }, dolly)
        document.search('#proposal-content/*').after(dolly)
      end
      document.search('#proposal-content/div:last()').last.remove
    end

    def assign_variables_to_node(variables, node)
      variables.each do |name, value|
        placeholder = "{#{name}}"
        nodes = node.search("//text()[contains(.,'#{placeholder}')]")
        nodes.each { |n| n.content = n.content.gsub(placeholder, value.to_s.html_safe) }
      end
    end
end
